require 'thread'
require 'net/http'

mutex = Mutex.new
condvar = ConditionVariable.new  # This must be shared amongst all threads
results = Array.new

# Put work in background so that the main thread can continue on
Thread.new do
  10.times do
    response = Net::HTTP.get_response('dynamic.xkcd.com', '/random/comic/')
    random_comic_url = response['Location']

    # Lock mutex so we can add to the shared array
    mutex.synchronize do
      results << random_comic_url
      condvar.signal  # Single the Conditional Variable - wakes a thread up
    end
  end
end

comics_received = 0

# Main thread will begin to start collecting results
until comics_received >= 10
  mutex.synchronize do
    while results.empty? # while loop is a fail safe; always recheck condition before proceding
      condvar.wait(mutex) # Put thread to sleep; needs a locked mutex
    end

    url = results.shift
    puts "You should check out #{url}"
  end

  # This += does not need a mutex since its only visable to one thread, the main thread.
  comics_received += 1
end
