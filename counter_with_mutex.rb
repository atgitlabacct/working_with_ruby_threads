@counter = 0
threads = []
mutex = Mutex.new # All threads must share same mutex

# Put as little code in your critical sections as possible
# Only modifiable data that could be shared by other threads

10.times do
  threads << Thread.new do
    mutex.synchronize do
      temp = @counter
      sleep(0.00001)
      temp = temp + 1
      @counter = temp
    end
  end
end

threads.each(&:join)
puts @counter
