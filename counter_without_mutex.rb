@counter = 0
threads = []

10.times do
  threads << Thread.new do
    temp = @counter
    sleep(0.00001)
    temp = temp + 1
    @counter = temp
  end
end

threads.each(&:join)
puts @counter
